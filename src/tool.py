#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  7 10:51:02 2023

@author: luca.genz
"""
# vim: set expandtab shiftwidth=4 softtabstop=4:


from chimerax.ui import HtmlToolInstance
import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__)))
import view
import methods
import html_clusters
import cluster_com
from Qt.QtWidgets import QVBoxLayout, QHBoxLayout, QGroupBox, QLabel
import scores


class PICKLUSTER(HtmlToolInstance):
    SESSION_ENDURING = False
    SESSION_SAVE = False  # No session saving for now
    CUSTOM_SCHEME = "pickluster"
    display_name = "pickluster" # HTML scheme for custom links
    PLACEMENT = None
    help = "help:user/tools/pickluster.html"
 
    
    
    def __init__(self,session,tool_name):

        super().__init__(session, tool_name, size_hint=(600, 1500))
        self.display_name = "pickluster"
        self._build_ui()
        self.view = view.PICKLUSTERView(self)
        self.view.render()



        
    def _build_ui(self):
        # Fill in html viewer with initial page in the module
        import os.path
        html_file = os.path.join(os.path.dirname(__file__), "template.html")
        import pathlib
        self.html_view.setUrl(pathlib.Path(html_file).as_uri())
        
    def handle_scheme(self, url):
        #print(url)

        command = url.path()
        from urllib.parse import parse_qs
        query = parse_qs(url.query())
        
        cutoff = float(query["cluster-cutoff"][0])
        chain1 = query["chain1"][0]
        chain2 = query["chain2"][0]
        index= int(query["model"][0]) -1
        model = self.session.models[index]
        key = f'{chain1}'+':'+f'{chain2}'
        clusters = []
        
        if command == "run":
            #print(url)
            if query["inputtype"][0] == 'structure':
                try: 
                    clusters, all_interact, int_allfunc, res_index, cmol_int = methods.intcluster(self.session, model, chain1, chain2, cutoff, 0,alphafold = False)
                    res_interactions, ato_residue_interactions = cluster_com.get_interaction_dics(all_interact, chain1, chain2)
                except:
                    self.session.logger.status('No interface found for '+f'{key}', log = True)
                    
                    
            if query["inputtype"][0] == 'alphafold':
                try:
                    cutoffplddt = float(query['plddtcutoff'][0])
                    clusters, all_interact, int_allfunc, res_index, cmol_int  = methods.intcluster(self.session, model, chain1, chain2, cutoff, cutoffplddt, alphafold = True)
                    res_interactions, ato_residue_interactions = cluster_com.get_interaction_dics(all_interact, chain1, chain2)
                except:
                    self.session.logger.status('No interface found for '+f'{key}', log = True) 
                                                                             
            if clusters:                                                                               
                if "plddt" in query:
                    methods.clusters_color_plddt(clusters)
                    methods.show_plddt_scale(self.session)
                    
                elif "Colorseq" in query:
                    m = methods.clusters_color(clusters)
                    path = str(query["path"][0])
                    methods.make_scf(m, path,model)
                    
                    
                    
                else:
                    methods.clusters_color(clusters)
                    
                if "resint" in query: 
                    html_clusters._report_cluster_interactions(self.session, model, chain1, chain2,clusters, res_interactions)
                    
                if "atmint" in query: 
                    methods.display_atomic_interactions(self.session, chain1, chain2, int_allfunc)
                    
                if "paescore" in query: 
                    pathscore = str(query["scorefile"][0])
                    scores.AlphaFoldScores(self.session, pathscore).plt_pae(model)
                    
                if "maxpae" in query: 
                    pathscore = str(query["scorefile"][0])
                    scores.AlphaFoldScores(self.session, pathscore).show_max_pae(model)
                    
                if "paescorecl" in query: 
                    pathscore = str(query["scorefile"][0])
                    scores.AlphaFoldScores(self.session, pathscore).calculate_median_pae_cluster(model, cmol_int, res_index)
                    
                if "ptm" in query: 
                    pathscore = str(query["scorefile"][0])
                    scores.AlphaFoldScores(self.session, pathscore).show_ptm(model)
                    
                if "iptm" in query: 
                    pathscore = str(query["scorefile"][0])
                    scores.AlphaFoldScores(self.session, pathscore).show_iptm(model)
                    
                if "meanplddt" in query: 
                    mean_plddt_dic = scores.Ind_scores(self.session).mean_plddt_per_cluster(clusters)
                    html_clusters. _report_mean_plddt(self.session, model, chain1, chain2,mean_plddt_dic)
        
                    

                
                

                
                
            
