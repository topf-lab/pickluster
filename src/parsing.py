#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  7 11:47:59 2023

@author: luca.genz
"""

import numpy as np

class Parsing_chimerax:
    
    def __init__(self, chimerax_model, plddtcutoff, alphafold = False):
        
        self.st = chimerax_model
        
        self.file_name = chimerax_model.name
        self.residue_ids = ["ALA", "ARG", "ASN" ,"ASP", "CYS", "GLU","GLN", "GLY", "HIS", "ILE","LEU", "LYS", 
                       "MET", "PHE", "PRO", "SER","THR", "TRP", "TYR","VAL"]
        self.pLDDT_cutoff = plddtcutoff
        
            
        all_atoms = []
        all_chains = []
        all_residues = {}
        all_atoms_allfunc = []
        res_index = {}
        
        if alphafold == False:
            
            for index, res in enumerate(self.st.residues): 
                res_index[res] = index
            
    
            for chain in self.st.chains:

                
                for residue in chain.residues:
                    if residue is not None:
                     
                        for atom in residue.atoms:
                            #try:
                            position = np.array([atom.coord[0],atom.coord[1],atom.coord[2]])
                            if atom.element.name != 'H' and residue.name in self.residue_ids and atom is not None: # and atom.b_iso >= self.pLDDT_cutoff:
                                atom_info = [residue.chain_id, residue.name, residue.number,
                                             atom.name,atom.serial_number, atom.element.name, position]
                                for_key = f'{residue.chain_id}:{residue.number}'
                                all_atoms.append(atom_info)
                                all_atoms_allfunc.append(atom)
                                if residue.chain_id not in all_chains:
                                    all_chains.append(residue.chain_id)
                                if for_key not in all_residues:
                                    all_residues[for_key] = [atom_info]
                                if for_key in all_residues:
                                    if atom_info not in all_residues[for_key]:
                                        all_residues[for_key].append(atom_info)
                            #except:
                             #   print('Chain '+f'{chain.chain_id}'+' contains NoneTypes')
                        
        if alphafold == True:
            
            for index, res in enumerate(self.st.residues): 
                res_index[res] = index
            
            for chain in self.st.chains:

                
                for residue in chain.residues:
                    if residue is not None:
                        #try: 
                        for atom in residue.atoms:
                            position = np.array([atom.coord[0],atom.coord[1],atom.coord[2]])
                            if atom.element.name != 'H' and residue.name in self.residue_ids and atom.bfactor >= self.pLDDT_cutoff and atom is not None:
                                atom_info = [residue.chain_id, residue.name, residue.number,
                                             atom.name,atom.serial_number, atom.element.name, position]
                                for_key = f'{residue.chain_id}:{residue.number}'
                                all_atoms.append(atom_info)
                                all_atoms_allfunc.append(atom)
                                if residue.chain_id not in all_chains:
                                    all_chains.append(residue.chain_id)
                                if for_key not in all_residues:
                                    all_residues[for_key] = [atom_info]
                                if for_key in all_residues:
                                    if atom_info not in all_residues[for_key]:
                                        all_residues[for_key].append(atom_info)
                       # except:
                        #    print('Chain '+f'{chain.chain_id}'+' contains NoneTypes')
                    
                            
   
        self.atoms = all_atoms
        self.chains = all_chains
        self.residue_dic = all_residues
        self.atoms_allfunc = all_atoms_allfunc
        self.res_index = res_index