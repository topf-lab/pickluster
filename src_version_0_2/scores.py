#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  3 15:43:55 2023

@author: luca.genz
"""

import json 
import numpy as np
from matplotlib import pyplot as plt 
import base64
from io import BytesIO
from chimerax.core.errors import UserError
import pickle

class Ind_scores():
    
    def __init__(self, session):
        self.session = session
        
    def mean_plddt_per_cluster(self,clusterdic):
        
        mean_plddt_dic = {}
        
        for index in clusterdic:
            count = 0
            sum_plddt = 0
            
            for res in clusterdic[index]:
                plddt = res.atoms[0].bfactor
                count += 1
                sum_plddt += plddt 
                
            mean_plddt_clus = round((sum_plddt/count),2) 
            
            mean_plddt_dic[index] = mean_plddt_clus
            
        return mean_plddt_dic
            


class AlphaFoldScores():
    
    def __init__(self, session, pathtofile):
        
        self.session = session
        self.path = pathtofile

    def plt_pae(self,modelnum):
        
        self.pae = []
        titlepae = "PAE for "+f'{modelnum.name}'
        
        
        with open(self.path) as f:
            data = json.load(f)
            try:
                for line in data['pae']:
                    self.pae.append(line)
            except: 
                try: 
                    for line in data['predicted_aligned_error']:
                        self.pae.append(line)
                except:
                    raise UserError('No PAE data')
                    

        ticks = np.arange(0, len(self.pae[0]),  200)
        fig = plt.figure(figsize=(6,6))
        PAE = plt.imshow(self.pae)
        plt.title(titlepae, loc = 'center', pad=10.0)
        plt.xticks(ticks, fontname="Sans")
        plt.yticks(ticks, fontname="Sans")
        plt.xlabel("Residue index", size=14, fontweight="bold", fontname="Sans")
        plt.ylabel("Residue index", size=14, fontweight="bold", fontname="Sans")
        scale = plt.colorbar(PAE, shrink=0.5)
        scale.set_label(label="Predicted aligned error (Å)",size=10, fontweight="bold", fontname="Sans")
        
        #from chimerax.core.logger import image_info_to_html
        #html_fig = 
        tmpfile = BytesIO()
        fig.savefig(tmpfile, format='png')
        encoded = base64.b64encode(tmpfile.getvalue()).decode('utf-8')
        
        html = '<img src=\'data:image/png;base64,{}\'>'.format(encoded) 
        self.session.logger.info(html, is_html=True)
        
    def show_max_pae(self, modelnum):
        
        self.max_pae = 0
        
        with open(self.path) as f:
            data = json.load(f)
            try:
                self.max_pae += data['max_pae']
            except: 
                try: 
                    self.max_pae += data['max_predicted_aligned_error']
                except:
                    raise UserError('No max. PAE data')
                    
        struct_name = modelnum.name
        info = 'Max. predicted aligned error for AlphaFold model '+f'{struct_name}'+': '+f'{round(self.max_pae,2)}' +' (Å)'
        
        self.session.logger.status(info, log = True)
        
    def show_ptm(self, modelnum):
        
        self.ptm = 0
        
        with open(self.path) as f:
            data = json.load(f)
            try:
                self.ptm += data['ptm']
            except: 
                raise UserError('No pTM data')
                    
        struct_name = modelnum.name
        info = 'pTM for AlphaFold model '+f'{struct_name}'+': '+f'{round(self.ptm,2)}' +' (Calculated by AlphaFold)'
        
        self.session.logger.status(info, log = True)
        
    def show_iptm(self, modelnum):
        
        self.iptm = 0
        
        with open(self.path) as f:
            data = json.load(f)
            try:
                self.iptm += data['iptm']
            except:
                raise UserError('No ipTM data')
                    
        struct_name = modelnum.name
        iptminfo = 'ipTM for AlphaFold model '+f'{struct_name}'+': '+f'{round(self.iptm,2)}' +' (Interface definition by AlphaFold)'
        
        self.session.logger.status(iptminfo, log = True)
        
    def show_confidence(self, modelnum):
        
        self.ciptm = 0
        self.cptm = 0
        
        with open(self.path) as f:
            data = json.load(f)
            try:
                self.ciptm += data['iptm']
                self.cptm += data['ptm']
            except:
                raise UserError('No ipTM data')
                raise UserError('No ipTM data')
                    
        struct_name = modelnum.name
        confidence = round((0.8*round(self.ciptm,2) + 0.2*round(self.cptm,2)),2)
        
        confidenceinfo = 'Confidence for AlphaFold model '+f'{struct_name}'+': '+f'{confidence}' +' (Interface definition by AlphaFold)'
        
        self.session.logger.status(confidenceinfo, log = True)
        
        
    def calculate_median_pae_cluster(self, modelnum, cmol_clusterdic, res_indices):
 
        self.pae_clus = []
        #start = modelnum.residues[0].number
        
        
        if self.path.endswith('json'):
            with open(self.path) as f:
                data = json.load(f)
                try:
                    self.pae_clus = data['pae']
                except: 
                    try: 
                        self.pae_clus = data['predicted_aligned_error']
                    except:
                        raise UserError('No PAE data')
                    
        if self.path.endswith('pkl'):
            with open(self.path, 'rb') as f:
                data = pickle.load(f)
                try:
                    self.pae_clus = data['pae']
                except: 
                    try: 
                        self.pae_clus = data['predicted_aligned_error']
                    except:
                        raise UserError('No PAE data')
                        
                        
                        
        for clus_num in cmol_clusterdic: 
            #pythondb.set_trace()
            inter = cmol_clusterdic[clus_num]
            count_mean_pae = 0
                
            for interaction in inter: 
                ind1 = res_indices[interaction[0]]
                ind2 = res_indices[interaction[1]]
                    

                count_mean_pae += (self.pae_clus[ind1][ind2]+self.pae_clus[ind2][ind1])
                    
                
            mean_pae = round(count_mean_pae/(len(inter)*2), 2)
            
            report_mean_pae = 'Mean pae for cluster '+f'{clus_num}'+': '+f'{mean_pae}' +' (Å)'
            self.session.logger.status(report_mean_pae, log = True)
            

                
                
        
        
                    
                    
        
        
        
        
    
        
        
        

        
        
                        
                
            
            
            

            
        