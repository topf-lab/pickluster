#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  7 09:14:56 2023

@author: luca.genz
"""
# vim: set expandtab shiftwidth=4 softtabstop=4:
    
from chimerax.core.toolshed import BundleAPI
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__)))

class _MyAPI(BundleAPI):

    api_version = 1     # register_command called with BundleInfo and
                        # CommandInfo instance instead of command name
                        # (when api_version==0)

    # Override method
    '''
    @staticmethod
    def register_command(bi, ci, logger):

        import tool
        if ci.name == 'intcluster':
            
            func = tool.intcluster
            desc = tool.intcluster_desc
            
            
        else:
            raise ValueError("trying to register unknown command: %s" % ci.name)

        if desc.synopsis is None:
            desc.synopsis = ci.synopsis

        
        from chimerax.core.commands import register
        register(ci.name, desc, func)
    '''
    # Override method
    @staticmethod
    def start_tool(session, bi, ti):

        #if ti.name == "PICKLUSTER":
        import tool
        return tool.PICKLUSTER(session, ti.name)
        #raise ValueError("trying to start unknown tool: %s" % ti.name)
        


# Create the ``bundle_api`` object that ChimeraX expects.
bundle_api = _MyAPI()