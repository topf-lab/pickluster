#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  7 10:51:02 2023

@author: luca.genz
"""
# vim: set expandtab shiftwidth=4 softtabstop=4:


from chimerax.ui import HtmlToolInstance
import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__)))
import view
import methods
import html_clusters
import cluster_com
from Qt.QtWidgets import QVBoxLayout, QHBoxLayout, QGroupBox, QLabel
import scores






class PICKLUSTER(HtmlToolInstance):
    SESSION_ENDURING = False
    SESSION_SAVE = False  # No session saving for now
    CUSTOM_SCHEME = "pickluster"
    display_name = "pickluster" # HTML scheme for custom links
    PLACEMENT = None
    help = "help:user/tools/pickluster.html"

 
    
    
    def __init__(self,session,tool_name):

        super().__init__(session, tool_name, size_hint=(600, 1500))
        self.display_name = "pickluster"
        self._build_ui()
        
        for model in self.session.models:
            model.clear_selection()

        
        
        all_chains = []
        all_chains_with_index = {}
        all_models = []
        all_modl_index = {}
        
        for index,model in enumerate(self.session.models): 
            if '.' not in model.id_string:
                all_models.append(f'{model.id_string}')
                all_modl_index[f'{model.id_string}'] = index
                
                for chain in model.chains: 
                    info = f'{model.id_string}:{chain.chain_id}'
                    all_chains.append(info)
                    all_chains_with_index[info] = index
                
        self.chains = all_chains
        self.chain_index = all_chains_with_index
        self.models = all_models
        self.model_index = all_modl_index
    
        self.view = view.PICKLUSTERView(self)
        self.view.render(self.chains, self.models)
        
        
        

    
    
    def _build_ui(self):
        # Fill in html viewer with initial page in the module
        import os.path
        html_file = os.path.join(os.path.dirname(__file__), "template.html")
        import pathlib
        self.html_view.setUrl(pathlib.Path(html_file).as_uri())
        
        
        
    def handle_scheme(self, url):
        #print(url)

        command = url.path()
        from urllib.parse import parse_qs
        query = parse_qs(url.query())
        
      
        
        
        
        if 'whole' in query: 
            
            index= self.model_index[query["models"][0]]
            model = self.session.models[index]
            
            all_keys = []
            
            
            for chain1 in model.chains: 
                for chain2 in model.chains: 
                    if chain1.chain_id != chain2.chain_id:   
                        for_key = sorted([chain1.chain_id,chain2.chain_id])
                        key = f'{for_key[0]}'+':'+f'{for_key[1]}'
                        if key not in all_keys:
                            all_keys.append(key)
                            
                            
            clusters_all = []
            clusters_plddt = []
            
            
                
                            
                            
            for key in all_keys:
                
                chain1 = key[0]
                chain2 = key[2]
               
                
                if query["clustering"][0] == 'with':    
                    cutoff = float(query["cluster-cutoff"][0])
                    
                elif query["clustering"][0] == 'without':           
                    cutoff = 1000000
                
                if command == "run":
                    #print(url)
                    if query["inputtype"][0] == 'structure':
                        try: 
                            clusters, all_interact, int_allfunc, res_index, cmol_int = methods.intcluster(self.session, model, chain1, chain2, cutoff, 0,alphafold = False)
                            res_interactions, ato_residue_interactions = cluster_com.get_interaction_dics(all_interact, chain1, chain2)
                        except:
                            self.session.logger.status('No interface found for '+f'{key}', log = True)
                            
                            
                    if query["inputtype"][0] == 'alphafold':
                        try:
                            cutoffplddt = float(query['plddtcutoff'][0])
                            clusters, all_interact, int_allfunc, res_index, cmol_int  = methods.intcluster(self.session, model, chain1, chain2, cutoff, cutoffplddt, alphafold = True)
                            res_interactions, ato_residue_interactions = cluster_com.get_interaction_dics(all_interact, chain1, chain2)
                        except:
                            self.session.logger.status('No interface found for '+f'{key}', log = True) 
                                                                                     
                    if clusters:       
                        
                        
                        
                        self.session.logger.status('Calculation of interface was successful', log = True)                                                                         
                        if "plddt" in query:
                            clusters_plddt.append(clusters)
                            
                            
                        elif "Colorseq" in query:
                            m = methods.clusters_color(clusters)
                            path = str(query["path"][0])
                            methods.make_scf(m, path,model)
                            
                            
                            
                        else:
                            clusters_all.append(clusters)
                            
                        if "resint" in query: 
                            html_clusters._report_cluster_interactions(self.session, model, chain1, chain2,clusters, res_interactions)
                            
                        if "atmint" in query: 
                            methods.display_atomic_interactions(self.session, chain1, chain2, int_allfunc)
                            
                        if "paescore" in query: 
                            pathscore = str(query["scorefile"][0])
                            scores.AlphaFoldScores(self.session, pathscore).plt_pae(model)
                            
                        if "maxpae" in query: 
                            pathscore = str(query["scorefile"][0])
                            scores.AlphaFoldScores(self.session, pathscore).show_max_pae(model)
                            
                        if "paescorecl" in query: 
                            pathscore = str(query["scorefile"][0])
                            scores.AlphaFoldScores(self.session, pathscore).calculate_median_pae_cluster(model, cmol_int, res_index)
                            
                        if "ptm" in query: 
                            pathscore = str(query["scorefile"][0])
                            scores.AlphaFoldScores(self.session, pathscore).show_ptm(model)
                            
                        if "iptm" in query: 
                            pathscore = str(query["scorefile"][0])
                            scores.AlphaFoldScores(self.session, pathscore).show_iptm(model)
                            
                        if "meanplddt" in query: 
                            mean_plddt_dic = scores.Ind_scores(self.session).mean_plddt_per_cluster(clusters)
                            html_clusters. _report_mean_plddt(self.session, model, chain1, chain2,mean_plddt_dic)
                            
            methods.clusters_color_complex(clusters_all)
            
            if clusters_plddt:
                methods.clusters_color_plddt_whole(clusters_plddt)
                methods.show_plddt_scale(self.session)
                            
                            
                        
                            
      

        
        else:    
            
            chain1 = query["chain1"][0].split(':')[1]
            chain2 = query["chain2"][0].split(':')[1]
            
            if query["chain1"][0] == query["chain2"][0]: 
                self.session.logger.status('Chain1 is equal to chain2!', log = True)
                
            elif query["chain1"][0].split(':')[0] != query["chain2"][0].split(':')[0]:
                self.session.logger.status('Selected chains are not in the same model!', log = True)
                
            else:
                
            
                if query["clustering"][0] == 'with':    
                    cutoff = float(query["cluster-cutoff"][0])
                    
                elif query["clustering"][0] == 'without':           
                    cutoff = 1000000
                    
                index= self.chain_index[query["chain1"][0]]
                model = self.session.models[index]
                key = f'{chain1}'+':'+f'{chain2}'
                clusters = []
                
                if command == "run":
                    #print(url)
                    if query["inputtype"][0] == 'structure':
                        try: 
                            clusters, all_interact, int_allfunc, res_index, cmol_int = methods.intcluster(self.session, model, chain1, chain2, cutoff, 0,alphafold = False)
                            res_interactions, ato_residue_interactions = cluster_com.get_interaction_dics(all_interact, chain1, chain2)
                        except:
                            self.session.logger.status('No interface found for '+f'{key}', log = True)
                            
                            
                    if query["inputtype"][0] == 'alphafold':
                        try:
                            cutoffplddt = float(query['plddtcutoff'][0])
                            clusters, all_interact, int_allfunc, res_index, cmol_int  = methods.intcluster(self.session, model, chain1, chain2, cutoff, cutoffplddt, alphafold = True)
                            res_interactions, ato_residue_interactions = cluster_com.get_interaction_dics(all_interact, chain1, chain2)
                        except:
                            self.session.logger.status('No interface found for '+f'{key}', log = True) 
                                                                                     
                    if clusters:       
                        self.session.logger.status('Calculation of interface was successful', log = True)                                                                         
                        if "plddt" in query:
                            methods.clusters_color_plddt(clusters)
                            methods.show_plddt_scale(self.session)
                            
                        elif "Colorseq" in query:
                            m = methods.clusters_color(clusters)
                            path = str(query["path"][0])
                            methods.make_scf(m, path,model)
                            
                            
                            
                        else:
                            methods.clusters_color(clusters)
                            
                        if "resint" in query: 
                            html_clusters._report_cluster_interactions(self.session, model, chain1, chain2,clusters, res_interactions)
                            
                        if "atmint" in query: 
                            methods.display_atomic_interactions(self.session, chain1, chain2, int_allfunc)
                            
                        if "paescore" in query: 
                            pathscore = str(query["scorefile"][0])
                            scores.AlphaFoldScores(self.session, pathscore).plt_pae(model)
                            
                        if "maxpae" in query: 
                            pathscore = str(query["scorefile"][0])
                            scores.AlphaFoldScores(self.session, pathscore).show_max_pae(model)
                            
                        if "paescorecl" in query: 
                            pathscore = str(query["scorefile"][0])
                            scores.AlphaFoldScores(self.session, pathscore).calculate_median_pae_cluster(model, cmol_int, res_index)
                            
                        if "ptm" in query: 
                            pathscore = str(query["scorefile"][0])
                            scores.AlphaFoldScores(self.session, pathscore).show_ptm(model)
                            
                        if "iptm" in query: 
                            pathscore = str(query["scorefile"][0])
                            scores.AlphaFoldScores(self.session, pathscore).show_iptm(model)
                            
                        if "confidence" in query: 
                            pathscore = str(query["scorefile"][0])
                            scores.AlphaFoldScores(self.session, pathscore).show_confidence(model)
                            
                        if "meanplddt" in query: 
                            mean_plddt_dic = scores.Ind_scores(self.session).mean_plddt_per_cluster(clusters)
                            html_clusters. _report_mean_plddt(self.session, model, chain1, chain2,mean_plddt_dic)
                            
      
                            
        
                        
                        
        
                        
                        
                    
