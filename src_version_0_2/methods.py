#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 27 11:00:04 2023

@author: luca.genz kuschelbär
"""


import colorsys
import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__)))
import cluster_com 
import parsing 
import html_clusters
from chimerax.core.errors import UserError
from matplotlib import pyplot as plt 
import base64
from io import BytesIO
import matplotlib as mpl



UNCLUSTERED_COLOR = [255, 255, 255, 255]

def get_n_colors_rgba(n):
    
    hsvs = [(x * 1.0 / n, 1.0, 1.0) for x in range(n)]
    colors = []
    for hsv in hsvs:
        rgb = colorsys.hsv_to_rgb(*hsv)
        rgba_u8 = (int(rgb[0] * 255), int(rgb[1] * 255), int(rgb[2] * 255), 255)
        colors.append(rgba_u8)
    return colors

def get_plddt_color(plddt_value):
    
    if plddt_value <50:
        color_plddt = (255, 0, 0, 255)
        
    if plddt_value >= 50 and plddt_value < 70:
        color_plddt = (255, 165, 0, 255)
        
    if plddt_value >= 70 and plddt_value < 80:
        color_plddt = (255, 255, 0, 255)
        
    if plddt_value >= 80 and plddt_value < 90:
        color_plddt = (0, 255, 255, 255)
        
    if plddt_value >= 90:
        color_plddt = (0, 0, 255, 255)

    return color_plddt

def get_bfact_color(bfactor):

    g = bfactor/100 *255
    b = (1 -bfactor/100) *255
    
    color_plddt = (0,g,b,255)
    return color_plddt
    
   

def intcluster(session, model, chain1, chain2, cluster_cutoff, plddt_cutoff, alphafold = False):
    
    if alphafold == False:
        a = parsing.Parsing_chimerax(model,0, alphafold = False)
    if alphafold == True: 
        a = parsing.Parsing_chimerax(model, plddt_cutoff, alphafold = True)
        
        
    atoms = a.atoms
    atoms_allfunc = a.atoms_allfunc
    interactions = cluster_com.calc_interactions_per_chain(atoms)
    interactions_allfunc = cluster_com.calc_interactions_per_chain_allfunc(atoms_allfunc)
    clustered_int_res = cluster_com.clustering_residue_based(interactions, a.residue_dic, cluster_cutoff)
    interfaces_incluster = cluster_com.interfaces(clustered_int_res)
    for_key = sorted([chain1, chain2])
    key = f'{for_key}' 
    cmol_interactions = cluster_com.res_interactions_all_functions(clustered_int_res, key, model)
    
    cluster_dictionary = {}
    clus_report = {}
    for chain in model.chains:
        for residue in chain.residues:
            if residue is not None:
            #try:
                residue.ribbon_color = UNCLUSTERED_COLOR
            #except: 
               # raise UserError('Chain '+f'{chain.chain_id}'+' contains NoneTypes')
    
    try: 
        for index, cluster in enumerate(interfaces_incluster[key]):
            for_dict = []
            for_report = []
            for chain in model.chains:
                for residue in chain.residues:
                    if residue is not None:
                    #try:
                        info = [residue.chain_id, residue.name, residue.number]
                    
                        if info in cluster:
    
                            if info not in for_report:
                                for_report.append(info)
                            
                            if residue not in for_dict:
                                for_dict.append(residue)
    
                            
                   # except: 
                        #raise UserError('Chain '+f'{chain.chain_id}'+' contains NoneTypes')
                        
            cluster_dictionary[index] = for_dict
            clus_report[index] = for_report
            
    except:
        session.logger.status('No interface found for '+f'{key}')
        
        
    html_clusters._report_cluster_summary(session, model, chain1, chain2, clus_report)
        
    return cluster_dictionary, interactions, interactions_allfunc, a.res_index, cmol_interactions
            
def clusters_color_complex(cluster_dic_all):
    
    count = 0
    for i in cluster_dic_all:
        count += len(i)

    colors = get_n_colors_rgba(count)
    
    color_seq = {}
    color_count= 0

    
    for interface in cluster_dic_all:
        
        
    
        for clus_num in interface: 
            residues = interface[clus_num]
            index= int(clus_num)+color_count
            
            
            for res in residues: 
                key_chain = f'{res.chain_id}'
                res.ribbon_color = colors[index]
                
                chain_for_index = res.chain
                number = chain_for_index.res_map[res]
               
                info = [number,0, colors[index][0], colors[index][1], colors[index][2]]
                
                if key_chain not in color_seq:
                    color_seq[key_chain] = [info]
                else:
                    color_seq[key_chain].append(info)
        color_count+= len(interface)
                
    return color_seq
            
def clusters_color(cluster_dic):
    
    colors = get_n_colors_rgba(len(cluster_dic))
    color_seq = {}
    
    for clus_num in cluster_dic: 
        residues = cluster_dic[clus_num]
        index= int(clus_num)
        
        
        for res in residues: 
            key_chain = f'{res.chain_id}'
            res.ribbon_color = colors[index]
            
            chain_for_index = res.chain
            number = chain_for_index.res_map[res]
           
            info = [number,0, colors[index][0], colors[index][1], colors[index][2]]
            
            if key_chain not in color_seq:
                color_seq[key_chain] = [info]
            else:
                color_seq[key_chain].append(info)
                
    return color_seq
                
def make_scf(color_dic, path_to_dir, model):
    #from pathlib import Path
    #home = Path.home()
    path = path_to_dir
    model_name = model.name
    for key in color_dic: 
        residues = color_dic[key]
        with open(path+f"{model_name}"+"_chain_"f"{key}"+"_cluster_coloring.scf", 'w') as f: 
            for res in residues: 
                for element in res: 
                    f.write(str(element) + ' ')
                f.write('\n')

            
def clusters_color_plddt(cluster_dic):
    
    for index in cluster_dic:
        residues = cluster_dic[index]
        
        for residue in residues: 
            for atom in residue.atoms: 
                plddt_score = atom.bfactor
                color = get_plddt_color(plddt_score)
                
                residue.ribbon_color = color
                
                
def clusters_color_plddt_whole(clusters_plddt):
    
    
    for cluster_dic in clusters_plddt:
        
        for index in cluster_dic:
            residues = cluster_dic[index]
            
            for residue in residues: 
                for atom in residue.atoms: 
                    plddt_score = atom.bfactor
                    color = get_plddt_color(plddt_score)
                    
                    residue.ribbon_color = color
                
def clusters_color_bfact(cluster_dic):
    
    for index in cluster_dic:
        residues = cluster_dic[index]
        
        for residue in residues: 
            for atom in residue.atoms: 
                bfact = atom.bfactor
                color = get_bfact_color(bfact)
                
                residue.ribbon_color = color
                
def display_atomic_interactions(session, chain1, chain2, int_dic):
    

    from chimerax.core.colors import BuiltinColors
    
    for_key = sorted([chain1, chain2])
    key = f'{for_key}' 
    
    name = f'Atomic interactions interface {key}'
    
    color =  'cornflowerblue'
    pbg = session.pb_manager.get_group(name)

    if key in int_dic:
    
        for interaction in int_dic[key]:
            
            pb = pbg.new_pseudobond((interaction[0]),( interaction[1]))
            pb.color = BuiltinColors[color].uint8x4()
            pb.radius = 0.075
            pb.dashes = 6


    session.models.add([pbg])
   
   
def show_plddt_scale(session):
    
    
    
    fig, ax = plt.subplots(figsize=(6, 1))
    fig.subplots_adjust(bottom=0.5)

    cmap = mpl.colors.ListedColormap(['red','orange', 'yellow','cyan',
                                      'blue'])
    #cmap.set_over('red')
    #cmap.set_under('blue')

    bounds = [0, 50, 70, 80, 90, 100]
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
    fig.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap),
                 cax=ax, orientation='horizontal',
                 label="pLDDT confidence measure (AlphaFold)")
        
    
    tmpfile = BytesIO()
    fig.savefig(tmpfile, format='png')
    encoded = base64.b64encode(tmpfile.getvalue()).decode('utf-8')
    
    html = '<img src=\'data:image/png;base64,{}\'>'.format(encoded) 
    session.logger.info(html, is_html=True)

    
        

        
                
                
                
                
        
        

