#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  9 10:30:35 2023

@author: luca.genz
"""
from chimerax.core import toolshed
from chimerax.core.models import Model
from chimerax.core.state import State
import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__)))





def _report_cluster_summary(session, modelnum, chain1, chain2, clusterdic):
    
    def sel_cluster(cluster, clusternum):
            return '<a title="Select cluster" href="cxcmd:select %s">%s</a>' % (
               (res[2] for res in cluster), clusternum)
        
    def make_selectable(clus, modnum):
        
        readable = ''
 
        for res in clus: 
            
            info =  '#'+f'{modnum}'+' /'+f'{res[0]}'+' :'+f'{res[2]}'+' '
            readable = readable+info
            
        return readable
            
   
    from chimerax.core.logger import html_table_params
    struct_name = modelnum.name
    chains = [chain1, chain2]
    header = 'Cluster information for '+f'{struct_name}'+f'{ chains}'
    lines = ['<table %s>' % html_table_params,
             '  <thead>',
             '    <tr>',
             '      <th colspan="2">'+ header + '</th>'
             '    </tr>',
             '    <tr>',
             '      <th>Cluster</th>',
             '      <th>Residues</th>',
             '    </tr>',
             '  </thead>',
             '  <tbody>',
    ]
    for cluster in clusterdic:
        
        res = clusterdic[cluster]
        
      
        
        c = cluster +1
        clus_key = f'{c}'
        residues = f'{res}'
        
        to_read = make_selectable(res, modelnum.id_string)
        command = '      <td style="text-align:center"><a href= "cxcmd:sel {}">{}</a></td>'.format(to_read,clus_key)
        
        
        lines.extend([
            '    <tr>',
            command,
            '      <td>' + residues + '</td>',
            '    </tr>',
        ])
    lines.extend(['  </tbody>',
                  '</table>'])
    summary = '\n'.join(lines)
    session.logger.info(summary, is_html=True)
    
    
def _report_cluster_interactions(session, modelnum, chain1, chain2, clusterdic, res_interactions):
    
   
    from chimerax.core.logger import html_table_params
    struct_name = modelnum.name
    chains = [chain1, chain2]
    header = 'Interactions in clusters in '+f'{struct_name}'+f'{ chains}'
    lines = ['<table %s>' % html_table_params,
             '  <thead>',
             '    <tr>',
             '      <th colspan="2">'+ header + '</th>'
             '    </tr>',
             '    <tr>',
             '      <th>Cluster</th>',
             '      <th>Interactions</th>',
             '    </tr>',
             '  </thead>',
             '  <tbody>',
    ]
    for cluster in clusterdic:
        
        interactions = []
        for res in clusterdic[cluster]:
            key =  f'{res.chain_id}:{res.number}'
            for res_int in res_interactions[key]:
                if res_int not in interactions: 
                    interactions.append(res_int)
                

        c = cluster +1
        clus_key = f'{c}'
        ints = f'{interactions}'
        lines.extend([
            '    <tr>',
            '      <td style="text-align:center">' + clus_key + '</td>',
            '      <td>' + ints + '</td>',
            '    </tr>',
        ])
    lines.extend(['  </tbody>',
                  '</table>'])
    summary = '\n'.join(lines)
    session.logger.info(summary, is_html=True)
    
def _report_mean_plddt(session, modelnum, chain1, chain2,plddt_dic):
    
   
    from chimerax.core.logger import html_table_params
    struct_name = modelnum.name
    chains = [chain1, chain2]
    header = 'Interactions in clusters in '+f'{struct_name}'+f'{ chains}'
    lines = ['<table %s>' % html_table_params,
             '  <thead>',
             '    <tr>',
             '      <th colspan="2">'+ header + '</th>'
             '    </tr>',
             '    <tr>',
             '      <th>Cluster</th>',
             '      <th>Interactions</th>',
             '    </tr>',
             '  </thead>',
             '  <tbody>',
    ]
    for cluster in plddt_dic:

        c = cluster +1
        clus_key = f'{c}'
        plddt = f'{plddt_dic[cluster]}'
        lines.extend([
            '    <tr>',
            '      <td style="text-align:center">' + clus_key + '</td>',
            '      <td>' + plddt + '</td>',
            '    </tr>',
        ])
    lines.extend(['  </tbody>',
                  '</table>'])
    summary = '\n'.join(lines)
    session.logger.info(summary, is_html=True)
    

    

    
    
    