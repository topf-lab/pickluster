#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 17 14:26:54 2023

@author: luca.genz
"""

"""The main HTML view of the RIBFIND app"""
from Qt.QtCore import QUrl
from jinja2 import Environment, PackageLoader, select_autoescape
import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__)))
from chimerax.atomic import AtomicStructure


class PICKLUSTERView:
    def __init__(self, tool):
        self.tool = tool
        self.html_view = tool.html_view

        env = Environment(
            loader=PackageLoader("chimerax.PICKLUSTER", '.'),
            autoescape=select_autoescape(),
        )
        self.template = env.get_template("template.html")

    def render(self,chains, models ):
        """Render the application.  Call this, when the application
        state changes."""
        dir_path = os.path.dirname(__file__)

        qurl = QUrl.fromLocalFile(dir_path)
        html = self.template.render(
            tool=self.tool, chain1 = chains, chain2 = chains, models = models
        )
        self.html_view.setHtml(html, qurl)
